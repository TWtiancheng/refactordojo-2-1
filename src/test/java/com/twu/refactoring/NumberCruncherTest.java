package com.twu.refactoring;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class NumberCruncherTest {

    @Test
    public void shouldCountEvenNumbers() {
        int evens = new NumberCruncher(Arrays.asList(new EvenCounter()),new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}).countEven();
        assertThat(evens, is(5));
    }

    @Test
    public void shouldCountOddNumbers() {
        int odds = new NumberCruncher(Arrays.asList(new EvenCounter(), new OddCounter()),new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}).countOdd();
        assertThat(odds, is(6));
    }

    @Test
    public void shouldCountPositiveNumbers() {
        int positives = new NumberCruncher(Arrays.asList(new EvenCounter(), new OddCounter(), new PositiveCounter()), new int[]{-5, -4, -3, -2, -1, 0, 1, 2, 3, 4}).countPositive();
        assertThat(positives, is(5));
    }

    @Test
    public void shouldCountNegativeNumbers() {
        int negatives = new NumberCruncher(Arrays.asList(new EvenCounter(), new OddCounter(), new PositiveCounter(),  new NegativeCounter()), new int[]{-5, -4, -3, -2, -1, 0, 1, 2, 3, 4}).countNegative();
        assertThat(negatives, is(5));
    }
}
