package com.twu.refactoring;

public class EvenCounter implements ICounter{

    @Override
    public int count(int[] numbers) {
        int countOfEvenNumbers = 0;

        for (int number : numbers) {
            if (number %2 == 0) {
                countOfEvenNumbers++;
            }
        }

        return countOfEvenNumbers;
    }

}
