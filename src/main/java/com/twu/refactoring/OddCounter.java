package com.twu.refactoring;

import java.util.Arrays;
import java.util.stream.Stream;

public class OddCounter implements ICounter{

    @Override
    public int count(int[] numbers) {
        int countOfOddNumbers = 0;

        for (int number : numbers) {
            if (number %2 != 0) {
                countOfOddNumbers++;
            }
        }

        return countOfOddNumbers;
    }
}
