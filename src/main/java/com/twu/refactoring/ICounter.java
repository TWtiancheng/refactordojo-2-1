package com.twu.refactoring;

public interface ICounter {

    int count(int[] numbers);
}
