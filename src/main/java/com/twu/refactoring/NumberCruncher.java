package com.twu.refactoring;

import java.util.Arrays;
import java.util.List;

public class NumberCruncher {
    private final int[] numbers;

    private List<ICounter> iCounterList;

    public NumberCruncher(List<ICounter> iCounterList, int[] numbers) {
        this.numbers = numbers;
        this.iCounterList = iCounterList;
    }

    public int countEven() {
        return iCounterList.get(0).count(numbers);
    }

    public int countOdd() {
        return iCounterList.get(1).count(numbers);
    }

    public int countPositive() {
        return iCounterList.get(2).count(numbers);
    }

    public int countNegative() {
        return iCounterList.get(3).count(numbers);
    }
}
