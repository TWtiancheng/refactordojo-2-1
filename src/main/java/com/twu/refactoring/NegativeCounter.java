package com.twu.refactoring;

public class NegativeCounter implements ICounter{

    @Override
    public int count(int[] numbers) {
        int countOfNegativeNumbers = 0;

        for (int number : numbers) {
            if (number %2 != 0) {
                countOfNegativeNumbers++;
            }
        }

        return countOfNegativeNumbers;
    }
}
