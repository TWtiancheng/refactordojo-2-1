package com.twu.refactoring;

import java.util.Arrays;
import java.util.stream.Stream;

public class PositiveCounter implements ICounter{

    @Override
    public int count(int[] numbers) {
        int countOfPositiveNumbers = 0;

        for (int number : numbers) {
            if (number % 2 != 0) {
                countOfPositiveNumbers++;
            }
        }

        return countOfPositiveNumbers;
    }
}
